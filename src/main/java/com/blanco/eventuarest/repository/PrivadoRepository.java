package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.dto.PrivadoDto;
import com.blanco.eventuarest.models.Privado;
import com.blanco.eventuarest.models.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PrivadoRepository extends CrudRepository<Privado,Long> {

    @Query(value="select * from privado where id_emisor=:emisor AND id_receptor=:receptor OR id_emisor=:receptor and id_receptor=:emisor", nativeQuery = true)
    public List<Privado> findMensajes(@Param("emisor")Long emisor, @Param("receptor")Long receptor);

    @Query(value="select * from privado where id_receptor=:receptor GROUP BY id_emisor", nativeQuery = true)
    public List<Privado> findLista1(@Param("receptor")Long receptor);

    @Query(value="select * from privado where id_emisor=:emisor GROUP BY id_receptor", nativeQuery = true)
    public List<Privado> findLista2(@Param("emisor")Long emisor);

    public List<Privado> findByLeidoAndEmisorAndReceptor(int leido, Usuario emisor, Usuario receptor);


}

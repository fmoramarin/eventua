package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.CP;
import com.blanco.eventuarest.models.Categoria;
import com.blanco.eventuarest.models.Evento;
import com.blanco.eventuarest.models.Usuario;
import org.apache.tomcat.jni.Local;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface EventoRepository extends CrudRepository<Evento, Long> {
    public List<Evento> findByCategoria(Categoria categoria);
    public List<Evento> findByCreador(Usuario usuario);
    public List<Evento> findByCp(CP cp);

    @Query(value = "select * from evento where evento.fecha_programada LIKE :fecha ' %'",nativeQuery = true)
    public List<Evento>filtroFecha(@Param("fecha")String fecha);

    public List<Evento> findByCoste (Double coste);
    @Query(value = "SELECT * FROM evento e WHERE e.coste > 0 AND e.coste <= :coste", nativeQuery = true)
    public List<Evento> filtroCoste(@Param("coste") Double coste);

    @Query(value = "SELECT * FROM evento WHERE evento.nombre LIKE  %:nombre%", nativeQuery = true)
    public List<Evento>filtroNombre(@Param("nombre")String nombre);
}

package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.Bloqueado;
import com.blanco.eventuarest.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BloqueadoRepository extends CrudRepository<Bloqueado, Long> {

    public Bloqueado findByBloqueadorAndBloqueado (Usuario bloqueador, Usuario bloqueado);
    public boolean existsByBloqueadorAndBloqueado (Usuario bloqueador, Usuario bloqueado);
    public Iterable<Bloqueado> findAllByBloqueador (Usuario bloqueador);

}

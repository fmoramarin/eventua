package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.Comentario;
import com.blanco.eventuarest.models.Evento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ComentarioRepository extends CrudRepository<Comentario, Long> {

    public List<Comentario> findByEvento (Evento evento);

}

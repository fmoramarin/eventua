package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.Premium;
import org.springframework.data.repository.CrudRepository;

public interface PremiumRepository extends CrudRepository<Premium,Long> {
    public Premium findByPlan(String plan);
}

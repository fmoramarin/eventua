package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.Interes;
import com.blanco.eventuarest.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InteresRepository extends CrudRepository<Interes, Long> {
    public List<Interes> findByUsuario(Usuario usuario);
}

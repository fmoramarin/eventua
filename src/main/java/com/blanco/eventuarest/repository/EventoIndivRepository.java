package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.Evento;
import com.blanco.eventuarest.models.Participante;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EventoIndivRepository extends CrudRepository<Evento, Long> {



}

package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.Evento;
import com.blanco.eventuarest.models.Favorito;
import com.blanco.eventuarest.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface FavoritoRepository extends CrudRepository<Favorito, Long> {
    @Transactional
    public Long deleteByUsuarioAndEvento(Usuario usuario, Evento evento);

    public Optional<Favorito> findByUsuarioAndEvento(Usuario usuario, Evento evento);

    public List<Favorito> findByUsuario (Usuario usuario);
}

package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.CP;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CPRepository extends CrudRepository<CP,String> {
public List<CP> findByProvincia(String provincia);
}

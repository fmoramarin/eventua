package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RegistroRepository extends CrudRepository<Usuario, Long> {
        public Optional<Usuario> findByUsername(String username);
        public Optional<Usuario> findByEmail(String email);
}

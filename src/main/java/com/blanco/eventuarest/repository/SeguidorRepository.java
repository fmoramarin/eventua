package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.Seguidor;

import com.blanco.eventuarest.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface SeguidorRepository extends CrudRepository<Seguidor, Long> {
    public List<Seguidor> findBySeguido (Usuario usuario);
    public List<Seguidor> findBySeguidor (Usuario usuario);
@Transactional
    public void deleteBySeguidorAndSeguido (Usuario seguidor, Usuario seguido);
    public Optional<Seguidor> findBySeguidorAndSeguido(Usuario seguidor, Usuario seguido);
}

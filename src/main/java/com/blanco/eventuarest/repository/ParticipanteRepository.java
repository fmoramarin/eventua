package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.Evento;
import com.blanco.eventuarest.models.Participante;
import com.blanco.eventuarest.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

public interface ParticipanteRepository extends CrudRepository<Participante, Long> {

    public List<Participante> findByAdmitidoAndEvento(int admitido, Evento evento);
    public Integer countByAdmitidoAndEvento(int admitido, Evento evento);
    public List<Participante> findByEvento (Evento evento);
    @Transactional
    public Long deleteByParticipanteAndEvento(Usuario usuario, Evento evento);

    public List<Participante> findByParticipante (Usuario usuario);

}

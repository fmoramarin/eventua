package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.models.Categoria;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoriaRepository extends CrudRepository<Categoria, Long> {
    public Categoria findBySubcategoria (String subcategoria);

    //public List<Categoria> findBySubcategoriaAndSubsubcategoria(String subcategoria, String subsubcategoria);
    //public List<Categoria> findByCategoriaAndSubsubcategoria(String categoria, String subsubcategoria);
}

package com.blanco.eventuarest.repository;

import com.blanco.eventuarest.dto.PerfilDto;
import com.blanco.eventuarest.models.CP;
import com.blanco.eventuarest.models.Evento;
import com.blanco.eventuarest.models.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
    //  @Query(value="SELECT user, email FROM usuario", nativeQuery = true)
    //@Query(value="SELECT u.user, u.email FROM Usuario u")
    //List<Usuario> getEmail();
    public Usuario findByUsername(String username);

    public List<Usuario> findByPremium(int premium);

    public List<Usuario> findByCp(CP cp);

    @Query(value = "SELECT * FROM usuario WHERE usuario.nombre LIKE  %:nombre% OR usuario.apellidos LIKE %:nombre%", nativeQuery = true)
    public List<Usuario>filtroNombre(@Param("nombre")String nombre);


}

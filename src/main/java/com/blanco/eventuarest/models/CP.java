package com.blanco.eventuarest.models;

import com.sun.istack.NotNull;
import javax.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "codigo_postal")
@Embeddable
public class CP implements Serializable {
    @Id
    @NotNull
    private String cp;

    @NotNull
    private String provincia;

    @NotNull
    @Column(name = "población")
    private String poblacion;


    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }
}

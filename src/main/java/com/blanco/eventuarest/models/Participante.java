package com.blanco.eventuarest.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "participante")
public class Participante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "fecha_inscripcion")
    private LocalDateTime fechaInscripcion;
    private int admitido;

    //RELACIONES CON OTRAS ENTIDADES
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario participante;

    @ManyToOne
    @JoinColumn(name = "id_evento")
    private Evento evento;


    //Constructor
    public Participante() {
    }


    //Getters y Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getFechaInscripcion() {
        return fechaInscripcion;
    }

    public void setFechaInscripcion(LocalDateTime fechaInscripcion) {
        this.fechaInscripcion = fechaInscripcion;
    }

    public int getAdmitido() {
        return admitido;
    }

    public void setAdmitido(int admitido) {
        this.admitido = admitido;
    }

    public Usuario getParticipante() {
        return participante;
    }

    public void setParticipante(Usuario participante) {
        this.participante = participante;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }
}

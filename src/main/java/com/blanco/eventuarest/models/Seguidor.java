package com.blanco.eventuarest.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="seguidor")
public class Seguidor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name= "id_seguidor")
    private Usuario seguidor;
    @ManyToOne
    @JoinColumn(name= "id_seguido")
    private Usuario seguido;
    private Date fecha;


    public Seguidor() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getSeguidor() {
        return seguidor;
    }

    public void setSeguidor(Usuario idSeguidor) {
        this.seguidor = idSeguidor;
    }

    public Usuario getSeguido() {
        return seguido;
    }

    public void setSeguido(Usuario idSeguido) {
        this.seguido = idSeguido;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}

package com.blanco.eventuarest.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.sun.istack.NotNull;
import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

    public Usuario() {}

    public Usuario(String cp, String username, String email, String password, String nombre, String apellidos, Date fechaNacimiento, String telefono, String imagen, String descripcion) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fechaNacimiento = fechaNacimiento;
        this.telefono = telefono;
        this.imagen = imagen;
        this.descripcion = descripcion;
        this.cp.setCp(cp);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private int rol;
    @NotNull
    private int estado;
    private int premium;
    @NotNull
    @Column(name="user")
    private String username;
    @NotNull
    @Column(unique = true)
    private String email;
    @NotNull
    private String password;
    @NotNull
    private String nombre;
    @NotNull
    private String apellidos;
    @NotNull
    @Column(name = "fecha_nacimiento")
    private Date fechaNacimiento;
    private String telefono;
    private String imagen;
    private String descripcion;

    @PrePersist
    public void prePersist() {
        fechaInscripcion = new Date();
    }

    @NotNull
    @Column(name = "fecha_inscripcion")
    private Date fechaInscripcion;
    @Column(name = "inicio_premium")
    private Date inicioPremium;
    @Column(name = "ultima_conexion")
    private LocalDateTime ultimaConexion;


    public void setUsername(String username) {
        this.username = username;
    }


    // RELACIONES CON OTRAS ENTIDADES
    @ManyToOne
    @JoinColumn(name = "cp")
    private CP cp;

    @ManyToOne
    @JoinColumn(name="plan_premium")
    private Premium planPremium;


// RELACIONES CON OTRAS ENTIDADES*/


    public CP getCp() {
        return cp;
    }

    public void setCp(CP cp) {
        this.cp = cp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public String getUsername() {
        return username;
    }

    public void setUser(String user) {
        this.username = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPremium() {
        return premium;
    }

    public void setPremium(int premium) {
        this.premium = premium;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaInscripcion() {
        return fechaInscripcion;
    }

    public void setFechaInscripcion(Date fechaInscripcion) {
        this.fechaInscripcion = fechaInscripcion;
    }

    public Date getInicioPremium() {
        return inicioPremium;
    }

    public void setInicioPremium(Date inicioPremium) {
        this.inicioPremium = inicioPremium;
    }

    public LocalDateTime getUltimaConexion() {
        return ultimaConexion;
    }

    public void setUltimaConexion(LocalDateTime ultimaConexion) {
        this.ultimaConexion = ultimaConexion;
    }

    public Premium getPlanPremium() {
        return planPremium;
    }

    public void setPlanPremium(Premium planPremium) {
        this.planPremium = planPremium;
    }
}

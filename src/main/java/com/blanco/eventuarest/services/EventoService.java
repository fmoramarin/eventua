package com.blanco.eventuarest.services;

import com.blanco.eventuarest.dto.BusquedaDto;
import com.blanco.eventuarest.dto.EventoDto;
import com.blanco.eventuarest.dto.EventoPerfilDto;
import com.blanco.eventuarest.models.CP;
import com.blanco.eventuarest.models.Categoria;
import com.blanco.eventuarest.models.Evento;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.EventoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class EventoService {

    @Autowired
    private EventoRepository eventoRepository;
    @Autowired
    private ParticipanteService participanteService;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private CategoriaService categoriaService;
    @Autowired
    private SeguidorService seguidorService;

    public void crearEvento(Evento evento) {
        eventoRepository.save(evento);
    }

    public List<Evento> findByCategoria(Categoria categoria) {
        return this.eventoRepository.findByCategoria(categoria);
    }

    public void borrarEvento(Evento evento) {
        eventoRepository.delete(evento);
    }

    public List<Evento> findByCreador(Usuario usuario) {
        return this.eventoRepository.findByCreador(usuario);
    }

    public List<Evento> findByCp (CP cp) {
        return this.eventoRepository.findByCp(cp);
    }

    public List<EventoDto> eventoAEventoDto (List<Evento> eventos) {

        List<EventoDto> eventosDto = new ArrayList<>();

        for (Evento evento : eventos) {
            EventoDto eventoDto = new EventoDto();
            eventoDto.setNombre(evento.getNombre());
            eventoDto.setImagen(evento.getImagen());
            eventoDto.setId(evento.getId());
            eventoDto.setFechaProgramada(evento.getFechaProgramada());
            eventoDto.setProvincia(evento.getCp().getProvincia());

            eventosDto.add(eventoDto);
        }
        return eventosDto;
    }

    public List<EventoDto> eventoRandom(List<EventoDto> eventoList) {
        List<EventoDto> eventos = new ArrayList<>();
       if (eventoList.size() > 4) {
            for (int i = 0; i < 4; i++) {
                Random random = new Random();
                int number = random.nextInt(eventoList.size());
                if (!eventos.contains(eventoList.get(number))){
                    eventos.add(eventoList.get(number));
                } else {
                    i--;
                }
            }
            return eventos;
        } else {
            return eventoList;
        }
    }

    public List<Evento> eventoRandomPremium(List<Evento> eventoList) {
        List<Evento> eventos = new ArrayList<>();
            for (int i = 0; i < eventoList.size(); i++) {
                Random random = new Random();
                int number = random.nextInt(eventoList.size());
                if (!eventos.contains(eventoList.get(number))){
                    eventos.add(eventoList.get(number));
                } else {
                    i--;
                }
            }
            return eventos;
    }

    public List<EventoPerfilDto> eventoAEventoPerfilDto (List<Evento> eventos) {
        List<EventoPerfilDto> eventosPerfilDto = new ArrayList<>();

        for (Evento evento : eventos) {
            EventoPerfilDto eventoPerfilDto = new EventoPerfilDto();

            eventoPerfilDto.setUsername(evento.getCreador().getUsername());
            eventoPerfilDto.setNombre(evento.getNombre());
            eventoPerfilDto.setImagen(evento.getImagen());
            eventoPerfilDto.setId(evento.getId());
            eventoPerfilDto.setFechaProgramada(evento.getFechaProgramada());
            eventoPerfilDto.setCoste(evento.getCoste());
            eventoPerfilDto.setCreador(evento.getCreador().getNombre() + " " + evento.getCreador().getApellidos());
            eventoPerfilDto.setCategoria(evento.getCategoria().getSubcategoria());
            eventoPerfilDto.setPoblacion(evento.getCp().getPoblacion());
            eventoPerfilDto.setMaxUsuarios(evento.getMaxUsuarios());
            eventoPerfilDto.setCountUsuarios(this.participanteService.totalAdmitidos(1, evento));

            eventosPerfilDto.add(eventoPerfilDto);
        }
        return eventosPerfilDto;
    }

    public Evento findById(Long id) {
        return eventoRepository.findById(id).orElse(null);
    }

    public List<Evento> findAll() {
        return (List<Evento>) this.eventoRepository.findAll();
    }

    public List<Evento> findByFecha(String fecha) {
        return this.eventoRepository.filtroFecha(fecha);
    }

    public List<Evento> findByCoste (Double coste) {
        return this.eventoRepository.findByCoste(coste);
    }

    public List<Evento> filtroCoste(Double coste) {
        return this.eventoRepository.filtroCoste(coste);
    }


    public List<Evento> filtroCategorias(BusquedaDto busquedaDto) {
        List<Evento> eventos = new ArrayList<>();

        for (String categoria : busquedaDto.getCategorias()) {
            Categoria cat = this.categoriaService.findBySubcategoria(categoria);
            List<Evento> eventosCat = this.findByCategoria(cat);
            eventos.addAll(eventosCat);
        }

        return eventos;
    }

    public List<Evento> filtroCp(List<Evento> eventos, CP cp) {
        List<Evento> eventosCp = new ArrayList<>();

        for (Evento evento : eventos) {
            if (evento.getCp() == cp) eventosCp.add(evento);
        }
        return eventosCp;
    }

    public List<Evento> eventosPorUsuario (List<Evento> eventos, int casoUsuario, Long idSesion) {
        Usuario seguidor = this.usuarioService.findById(idSesion);
        List<Evento> eventosPorUsuario = new ArrayList<>();
        switch (casoUsuario) {
            case 1:
                for (Evento evento : eventos) {
                    if (evento.getCreador().getPremium() == 1) {
                        eventosPorUsuario.add(evento);
                    }
                }
                break;

            case 2:
                for (Evento evento : eventos) {
                    if (this.seguidorService.findBySeguidorAndSeguido(seguidor,evento.getCreador()).isPresent()) {
                        eventosPorUsuario.add(evento);
                    }
                }
                break;

            default:
                eventosPorUsuario.addAll(eventos);
                break;
        }
        return eventosPorUsuario;
    }

    public List<Evento> findByNombre(String nombre) {
        return this.eventoRepository.filtroNombre(nombre);
    }
}

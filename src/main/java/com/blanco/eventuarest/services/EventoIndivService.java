package com.blanco.eventuarest.services;

import com.blanco.eventuarest.models.Evento;
import com.blanco.eventuarest.repository.EventoIndivRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventoIndivService {

    @Autowired
    private EventoIndivRepository eventoIndivRepository;

    public Evento findById(Long id) {
        return this.eventoIndivRepository.findById(id).orElse(null);
    }

}

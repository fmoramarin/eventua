package com.blanco.eventuarest.services;

import com.blanco.eventuarest.dto.FavoritoDto;
import com.blanco.eventuarest.dto.ParticipanteDto;
import com.blanco.eventuarest.models.Evento;
import com.blanco.eventuarest.models.Favorito;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.EventoRepository;
import com.blanco.eventuarest.repository.FavoritoRepository;
import com.blanco.eventuarest.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class FavoritoService {
    @Autowired
    FavoritoRepository favoritoRepository;
    @Autowired
    UsuarioRepository usuarioRepository;
    @Autowired
    EventoRepository eventoRepository;

    public Favorito save(FavoritoDto favoritoDto) {
        Favorito favorito = new Favorito();
        Usuario usuario = new Usuario();
        usuario.setId(favoritoDto.getIdUsuario());
        Evento evento = new Evento();
        evento.setId(favoritoDto.getIdEvento());
        favorito.setUsuario(usuario);
        favorito.setEvento(evento);
        favorito.setFecha(new Date());
        return this.favoritoRepository.save(favorito);
    }

    public Long retirarFav(FavoritoDto favoritoDto) {
        Usuario usuario = this.usuarioRepository.findById(favoritoDto.getIdUsuario()).orElse(null);
        Evento evento = this.eventoRepository.findById(favoritoDto.getIdEvento()).orElse(null);
        return this.favoritoRepository.deleteByUsuarioAndEvento(usuario,evento);
    }

    public Optional<Favorito> findFav(Usuario usuario, Evento evento) {
        Optional<Favorito> fav = this.favoritoRepository.findByUsuarioAndEvento(usuario, evento);
        return fav;
    }

    public List<Favorito> listByUsuario (Usuario usuario) {
        return this.favoritoRepository.findByUsuario(usuario);
    }
}

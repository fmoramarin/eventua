package com.blanco.eventuarest.services;


import com.blanco.eventuarest.models.Bloqueado;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.BloqueadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BloqueadoService {

    @Autowired
    private BloqueadoRepository bloqueadoRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private SeguidorService seguidorService;

    public void bloquear (String bloqueador, String bloqueado) {
        Bloqueado bloqueadoObj = new Bloqueado();
        bloqueadoObj.setBloqueador(this.usuarioService.findByUsername(bloqueador));
        bloqueadoObj.setBloqueado(this.usuarioService.findByUsername(bloqueado));
        bloqueadoObj.setFechaBloqueo(new Date());

        this.seguidorService.deleteBySeguidorAndSeguido(this.usuarioService.findByUsername(bloqueador),this.usuarioService.findByUsername(bloqueado));
        this.seguidorService.deleteBySeguidorAndSeguido(this.usuarioService.findByUsername(bloqueado),this.usuarioService.findByUsername(bloqueador));
        this.bloqueadoRepository.save(bloqueadoObj);
    }

    public boolean buscarBloqueo (String bloqueador, String bloqueado) {
        Usuario usuBloqueador = usuarioService.findByUsername(bloqueador);
        Usuario usuBloqueado = usuarioService.findByUsername(bloqueado);

        return this.bloqueadoRepository.existsByBloqueadorAndBloqueado(usuBloqueador, usuBloqueado);

    }

    public void desbloquear (String bloqueador, String bloqueado) {
        Usuario usuBloqueador = usuarioService.findByUsername(bloqueador);
        Usuario usuBloqueado = usuarioService.findByUsername(bloqueado);
        Bloqueado bloqueadoObj = this.bloqueadoRepository.findByBloqueadorAndBloqueado(usuBloqueador, usuBloqueado);

        this.bloqueadoRepository.delete(bloqueadoObj);
    }

    public List<Bloqueado> listarBloqueados (String bloqueador) {
        return (List<Bloqueado>) this.bloqueadoRepository.findAllByBloqueador(this.usuarioService.findByUsername(bloqueador));
    }

}

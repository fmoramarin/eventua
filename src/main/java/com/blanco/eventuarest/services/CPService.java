package com.blanco.eventuarest.services;

import com.blanco.eventuarest.models.CP;
import com.blanco.eventuarest.repository.CPRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CPService {
    @Autowired
    CPRepository cpRepository;

    public boolean buscarCP(String cp) {
        Optional<CP> cpOpt = this.cpRepository.findById(cp);
        if (cpOpt.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    public List<CP> findByProvincia(String provincia) {
        return this.cpRepository.findByProvincia(provincia);
    }
    public CP findById(String id) {
         return this.cpRepository.findById(id).orElse(null);
    }
}

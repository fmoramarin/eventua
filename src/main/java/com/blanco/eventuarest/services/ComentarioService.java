package com.blanco.eventuarest.services;

import com.blanco.eventuarest.dto.ComentarioDto;
import com.blanco.eventuarest.models.Comentario;
import com.blanco.eventuarest.models.Evento;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.ComentarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComentarioService {

    @Autowired
    private ComentarioRepository comentarioRepository;

    public List<Comentario> findByEvento(Evento evento) {
        return this.comentarioRepository.findByEvento(evento);
    }

    public void publicarComentario(ComentarioDto comentarioDto) {
        Comentario comentario = new Comentario();
        Usuario usuario = new Usuario();
        usuario.setId(comentarioDto.getIdUsuario());

        Evento evento = new Evento();
        evento.setId(comentarioDto.getIdEvento());

        comentario.setUsuario(usuario);
        comentario.setEvento(evento);
        comentario.setComentario(comentarioDto.getComentario());
        comentario.setFecha(comentarioDto.getFecha());
        comentario.setReportes(0);
        this.comentarioRepository.save(comentario);
    }

    public void save(Comentario comentario) {
        this.comentarioRepository.save(comentario);
    }

    public Comentario findById (Long id) {return this.comentarioRepository.findById(id).orElse(null);}
}

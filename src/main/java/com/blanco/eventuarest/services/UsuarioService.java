package com.blanco.eventuarest.services;

import com.blanco.eventuarest.dto.PerfilDto;
import com.blanco.eventuarest.models.CP;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService{
    @Autowired
    private UsuarioRepository usuarioRepository;

    public List<Usuario> findAll() {
        return (List<Usuario>) usuarioRepository.findAll();
    }

    public Usuario findByUsername(String username) {
        return usuarioRepository.findByUsername(username);
    }
    public Usuario findById(Long id) {
        return usuarioRepository.findById(id).orElse(null);
    }

    public Usuario save(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    public void delete(Long id) {
        usuarioRepository.deleteById(id);
    }

    public List<Usuario> findByPremium (int premium) {
        return this.usuarioRepository.findByPremium(premium);
    }

    public List<Usuario> findByCp (CP cp) {
        return this.usuarioRepository.findByCp(cp);
    }

    public List<PerfilDto> findByNombreAndApellidos(String nombre) {
        List<Usuario> usuarios = this.usuarioRepository.filtroNombre(nombre);
        List<PerfilDto> perfilDtos = new ArrayList<>();
        for (Usuario usuario: usuarios) {
            PerfilDto perfilDto = new PerfilDto();
            perfilDto.setUsername(usuario.getUsername());
            perfilDto.setNombre(usuario.getNombre());
            perfilDto.setApellidos(usuario.getApellidos());
            perfilDto.setImagen(usuario.getImagen());
            if (usuario.getRol() != 0 && usuario.getRol() != 1) {
                perfilDtos.add(perfilDto);
            }

        }
        return perfilDtos;
    }
}

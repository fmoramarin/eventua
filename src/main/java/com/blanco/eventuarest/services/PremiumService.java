package com.blanco.eventuarest.services;

import com.blanco.eventuarest.dto.PremiumDto;
import com.blanco.eventuarest.models.Premium;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.PremiumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PremiumService {
    @Autowired
    PremiumRepository premiumRepository;
    @Autowired
    UsuarioService usuarioService;

    public Premium findByPlan(String plan) {
        return this.premiumRepository.findByPlan(plan);
    }

    public void serPremium(PremiumDto premiumDto) {
        Usuario usuario = this.usuarioService.findById(premiumDto.getIdUsuario());
        Premium premium = this.premiumRepository.findById(premiumDto.getIdPremium()).orElse(null);
        usuario.setPremium(1);
        usuario.setInicioPremium(new Date());
        usuario.setPlanPremium(premium);
        this.usuarioService.save(usuario);
    }
}

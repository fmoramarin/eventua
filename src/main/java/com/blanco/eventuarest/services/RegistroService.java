package com.blanco.eventuarest.services;

import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.RegistroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class RegistroService {
    @Autowired
    RegistroRepository registroRepository;

    public boolean comprobarUsername(String username) {
        Optional<Usuario> usuarioOpt = this.registroRepository.findByUsername(username);
        if (usuarioOpt.isPresent()) return true;
        else return false;
    }

    public boolean comprobarEmail(String email) {
        Optional<Usuario> usuarioOpt = this.registroRepository.findByEmail(email);
        if (usuarioOpt.isPresent()) return true;
        else return false;
    }
}

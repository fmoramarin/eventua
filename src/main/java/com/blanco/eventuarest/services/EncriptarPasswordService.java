package com.blanco.eventuarest.services;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Base64;

public class EncriptarPasswordService {

    public static Key getKey(String pass) {
        final String KEY = "/idopn$_fu8ro5so48iopsj159ie_)ei";
        String llave = pass.concat(KEY.substring(pass.length()));
        Key key = new SecretKeySpec(llave.getBytes(), "AES");
        return key;
    }

    public static String encriptar(String pass) throws Exception {
        Cipher cifrado = Cipher.getInstance("AES");
        cifrado.init(Cipher.ENCRYPT_MODE, getKey(pass));
        byte[] encriptado = cifrado.doFinal(pass.getBytes());
        return Base64.getEncoder().encodeToString(encriptado);
    }

    public static String desencriptar(String code) throws Exception {
        Cipher cifrado = Cipher.getInstance("AES");
        cifrado.init(Cipher.DECRYPT_MODE, getKey(code));
        byte[] bytesDecodificados = Base64.getDecoder().decode(code);
        return new String(cifrado.doFinal(bytesDecodificados));
    }

    public static void main(String[] args) throws Exception {
        System.out.println(encriptar("57Ma1996*"));
    }

}

package com.blanco.eventuarest.services;

//import com.blanco.eventuarest.models.LoginForm;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class LoginService {
   // @Autowired
   // private LoginRepository loginRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;

    //public List<Login> getLogin() {
     ///   return (List<Login>) loginRepository.findAll();
 //   }

    public Map<String, Object> verificarLogin(Usuario usuario) {
        Iterable<Usuario> usuarios = this.usuarioRepository.findAll();
        Map<String, Object> respuesta = new HashMap<>();
        respuesta.put("idUsuario",-1);
        usuarios.forEach(usu -> {
            try {
                if (usu.getUsername().equals(usuario.getUsername()) && usu.getPassword().equals(EncriptarPasswordService.encriptar(usuario.getPassword()))) {
                    if (usu.getEstado() == 1) {
                        respuesta.put("idUsuario",usu.getId());
                        respuesta.put("rol",usu.getRol());
                        respuesta.put("premium",usu.getPremium());
                        respuesta.put("nombre",usu.getNombre());
                        respuesta.put("username",usu.getUsername());
                    } else {
                        respuesta.put("idUsuario",-1);
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        return respuesta;
    }
}

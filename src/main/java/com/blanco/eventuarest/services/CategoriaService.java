package com.blanco.eventuarest.services;

import com.blanco.eventuarest.models.Categoria;
import com.blanco.eventuarest.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriaService {
    @Autowired
    CategoriaRepository categoriaRepository;

    public List<Categoria> getCategorias() {
        return (List<Categoria>) this.categoriaRepository.findAll();
    }
    public Categoria getCategoria(Long id) {
        return this.categoriaRepository.findById(id).orElse(null);
    }
    public Categoria findBySubcategoria(String subcategoria) {return this.categoriaRepository.findBySubcategoria(subcategoria);}

   /* public List<Categoria> listarCategorias() {
        return this.categoriaRepository.findBySubcategoriaAndSubsubcategoria("","");
    }

    public List<Categoria> listarSubCategorias(String categoria) {
        return this.categoriaRepository.findByCategoriaAndSubsubcategoria(categoria,"");
    }*/
}

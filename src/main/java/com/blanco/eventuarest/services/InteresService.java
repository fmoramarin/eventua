package com.blanco.eventuarest.services;

import com.blanco.eventuarest.models.Interes;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.InteresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InteresService {
    @Autowired
    InteresRepository interesRepository;

    public Iterable<Interes> saveAll(List<Interes> intereses) {
        return interesRepository.saveAll(intereses);
    }

    public List<Interes> findByUsuario (Usuario usuario) {
        return interesRepository.findByUsuario(usuario);
    }
}

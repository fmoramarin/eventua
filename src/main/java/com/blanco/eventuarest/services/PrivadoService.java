package com.blanco.eventuarest.services;

import com.blanco.eventuarest.dto.PrivadoDto;
import com.blanco.eventuarest.models.Privado;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.PrivadoRepository;
import com.blanco.eventuarest.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class PrivadoService {

    @Autowired
    PrivadoRepository privadoRepository;
    @Autowired
    UsuarioRepository usuarioRepository;

    public List<PrivadoDto> getMensajes(Long emisor,Long receptor) {

        List<PrivadoDto> mensajesEmisor = new ArrayList<>();
        List<Privado> emisorAReceptor = this.privadoRepository.findMensajes(emisor, receptor);

        for (Privado privado:emisorAReceptor) {
            PrivadoDto privadoDto = new PrivadoDto();
            privadoDto.setNombre(privado.getEmisor().getNombre());
            privadoDto.setApellidos(privado.getEmisor().getApellidos());
            privadoDto.setId(privado.getEmisor().getId());
            privadoDto.setImagen(privado.getEmisor().getImagen());
            privadoDto.setMensajesEmisor(privado.getMensaje());
            privadoDto.setFechaMensaje(privado.getFecha());
            privadoDto.setUltimaConexion(privado.getEmisor().getUltimaConexion());
            privadoDto.setLeido(privado.getLeido());
            mensajesEmisor.add(privadoDto);
        }
        return mensajesEmisor;
    }

    public List<PrivadoDto> getChat(Long emisor) {
        List<Privado> listaMensajes1 = this.privadoRepository.findLista1(emisor);
        List<Privado> listaMensajes2 = this.privadoRepository.findLista2(emisor);
        List<PrivadoDto> listaCompleta = new ArrayList<>();

        for (Privado privado:listaMensajes1) {
            PrivadoDto privadoDto = new PrivadoDto();
            privadoDto.setNombre(privado.getEmisor().getNombre());
            privadoDto.setApellidos(privado.getEmisor().getApellidos());
            privadoDto.setId(privado.getEmisor().getId());
            privadoDto.setImagen(privado.getEmisor().getImagen());
            privadoDto.setUltimaConexion(privado.getEmisor().getUltimaConexion());
            privadoDto.setLeido(privado.getLeido());
            listaCompleta.add(privadoDto);
        }

        for (Privado privado:listaMensajes2) {
            boolean esta = false;
            PrivadoDto privadoDto = new PrivadoDto();
            privadoDto.setNombre(privado.getReceptor().getNombre());
            privadoDto.setApellidos(privado.getReceptor().getApellidos());
            privadoDto.setId(privado.getReceptor().getId());
            privadoDto.setImagen(privado.getReceptor().getImagen());
            privadoDto.setUltimaConexion(privado.getReceptor().getUltimaConexion());
            privadoDto.setLeido(privado.getLeido());
            System.out.println(privado.getLeido());

            for (PrivadoDto lista:listaCompleta) {
                if (lista.getId().equals(privadoDto.getId())) {
                    esta = true;
                }
            }
                if (!esta) listaCompleta.add(privadoDto);
        }

            return listaCompleta;
    }

    public void guardarMensaje(PrivadoDto privadoDto) {
        Privado privado = new Privado();
        Usuario emisor = this.usuarioRepository.findById(privadoDto.getId()).orElse(null);
        Usuario receptor = this.usuarioRepository.findById(privadoDto.getIdReceptor()).orElse(null);
        privado.setMensaje(privadoDto.getMensajesEmisor());
        privado.setEmisor(emisor);
        privado.setReceptor(receptor);
        privado.setFecha(LocalDateTime.now());
        privado.setLeido(0);
        this.privadoRepository.save(privado);
    }

    public void mensajesLeidos(PrivadoDto privadoDto) {
        Usuario emisor = this.usuarioRepository.findById(privadoDto.getId()).orElse(null);
        Usuario receptor = this.usuarioRepository.findById(privadoDto.getIdReceptor()).orElse(null);

        List<Privado> privados = this.privadoRepository.findByLeidoAndEmisorAndReceptor(0, emisor, receptor);

        for (Privado privado:privados) {
            privado.setLeido(1);
        }

        this.privadoRepository.saveAll(privados);
    }

}

package com.blanco.eventuarest.services;

import com.blanco.eventuarest.dto.ParticipanteDto;
import com.blanco.eventuarest.models.Evento;
import com.blanco.eventuarest.models.Participante;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.EventoRepository;
import com.blanco.eventuarest.repository.ParticipanteRepository;
import com.blanco.eventuarest.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParticipanteService {

    @Autowired
    private ParticipanteRepository participanteRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private EventoRepository eventoRepository;

    public Integer totalAdmitidos(int admitido, Evento evento) {
        return this.participanteRepository.countByAdmitidoAndEvento(admitido, evento);
    }

    public Participante save(ParticipanteDto participanteDto) {
        Participante participante = new Participante();
        Usuario usuario = new Usuario();
        usuario.setId(participanteDto.getIdUsuario());
        Evento evento = new Evento();
        evento.setId(participanteDto.getIdEvento());
        participante.setParticipante(usuario);
        participante.setEvento(evento);
        participante.setFechaInscripcion(participanteDto.getFecha());
        participante.setAdmitido(0);
        return this.participanteRepository.save(participante);
    }

    public Long retirarse(ParticipanteDto participanteDto) {
        Usuario usuario = this.usuarioRepository.findById(participanteDto.getIdUsuario()).orElse(null);
        Evento evento = this.eventoRepository.findById(participanteDto.getIdEvento()).orElse(null);
        return this.participanteRepository.deleteByParticipanteAndEvento(usuario,evento);
    }

    public List<Participante> findByEvento (Evento evento) {
        return this.participanteRepository.findByEvento(evento);
    }

    public List<Participante> findByAdmitidoAndEvento(int admitido, Evento evento) {
        return this.participanteRepository.findByAdmitidoAndEvento(admitido, evento);
    }

    public void aceptarParticipante(Long idParticipante) {
        Participante participante = this.participanteRepository.findById(idParticipante).orElse(null);
        participante.setAdmitido(1);
        this.participanteRepository.save(participante);
    }

    public void eliminarParticipante(Long idParticipante) {
        Participante participante = this.participanteRepository.findById(idParticipante).orElse(null);
        this.participanteRepository.delete(participante);
    }

    public List<Participante> findByParticipante(Usuario usuario) {
        return this.participanteRepository.findByParticipante(usuario);
    }

}

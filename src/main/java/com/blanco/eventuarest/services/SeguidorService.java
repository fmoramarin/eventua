package com.blanco.eventuarest.services;

import com.blanco.eventuarest.models.Seguidor;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.repository.SeguidorRepository;
import com.blanco.eventuarest.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SeguidorService {

    @Autowired
    private SeguidorRepository seguidorRepository;
    @Autowired
    UsuarioService usuarioService;

    public List<Seguidor> findBySeguido (Usuario usuario) {
        return this.seguidorRepository.findBySeguido(usuario);
    }

    public List<Seguidor> findBySeguidor (Usuario usuario) {
        return this.seguidorRepository.findBySeguidor(usuario);
    }

    public void save (String userSeguidor, String userSeguido) {

        Usuario usuario1 = usuarioService.findByUsername(userSeguidor);
        Usuario usuario2 = usuarioService.findByUsername(userSeguido);
        Seguidor seguidor = new Seguidor();
        seguidor.setSeguidor(usuario1);
        seguidor.setSeguido(usuario2);
        seguidor.setFecha(new Date());

        seguidorRepository.save(seguidor);
    }

    public void deleteBySeguidorAndSeguido (Usuario seguidor, Usuario seguido) {
        this.seguidorRepository.deleteBySeguidorAndSeguido(seguidor, seguido);
    }

    public Optional<Seguidor> findBySeguidorAndSeguido (Usuario seguidor, Usuario seguido) {
        return this.seguidorRepository.findBySeguidorAndSeguido(seguidor,seguido);
    }
}

package com.blanco.eventuarest.dto;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;

public class BusquedaDto {
    private String[] categorias;
    private Double coste;
    private String cp;
    private String provincia;
    private String fechaProgramada;
    private int casoUsuario;
    private Long idSesion;


    public BusquedaDto() {
    }

    public String[] getCategorias() {
        return categorias;
    }

    public void setCategorias(String[] categorias) {
        this.categorias = categorias;
    }

    public Double getCoste() {
        return coste;
    }

    public void setCoste(Double coste) {
        this.coste = coste;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getFechaProgramada() {
        return fechaProgramada;
    }

    public void setFechaProgramada(String fechaProgramada) {
        this.fechaProgramada = fechaProgramada;
    }

    public int getCasoUsuario() {
        return casoUsuario;
    }

    public void setCasoUsuario(int casoUsuario) {
        this.casoUsuario = casoUsuario;
    }

    public Long getIdSesion() {
        return idSesion;
    }

    public void setIdSesion(Long idSesion) {
        this.idSesion = idSesion;
    }

    @Override
    public String toString() {
        return "BusquedaDto{" +
                "categorias=" + Arrays.toString(categorias) +
                ", coste=" + coste +
                ", cp='" + cp + '\'' +
                ", provincia='" + provincia + '\'' +
                ", fechaProgramada=" + fechaProgramada +
                ", casoUsuario=" + casoUsuario +
                ", idSesion=" + idSesion +
                '}';
    }
}

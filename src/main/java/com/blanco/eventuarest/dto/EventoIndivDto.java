package com.blanco.eventuarest.dto;

import com.blanco.eventuarest.models.CP;
import com.blanco.eventuarest.models.Categoria;
import com.blanco.eventuarest.models.Usuario;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class EventoIndivDto {

    private Long id;
    private String nombre;
    private String imagen;
    private String descripcion;
    private Date fechaCreacion;
    private LocalDateTime fechaProgramada;
    private LocalDateTime fechaLimite;
    private String direccion;
    private Double coste;
    private Integer maxUsuarios;
    private Integer countUsuarios;
    private String nivel;
    private String categoria;
    private String poblacion;
    private String provincia;

    private Long idCreador;
    private String usernameCreador;
    private Integer premiumCreador;
    private String nombreCreador;
    private String apellidosCreador;
    private String imagenCreador;

    private List<ParticipanteDto> participantes;

    private List<ComentarioDto> comentarios;


    //Constructor
    public EventoIndivDto() {

    }


    //Getters y Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public LocalDateTime getFechaProgramada() {
        return fechaProgramada;
    }

    public void setFechaProgramada(LocalDateTime fechaProgramada) {
        this.fechaProgramada = fechaProgramada;
    }

    public LocalDateTime getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(LocalDateTime fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Double getCoste() {
        return coste;
    }

    public void setCoste(Double coste) {
        this.coste = coste;
    }

    public Integer getMaxUsuarios() {
        return maxUsuarios;
    }

    public void setMaxUsuarios(Integer maxUsuarios) {
        this.maxUsuarios = maxUsuarios;
    }


    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Long getIdCreador() {
        return idCreador;
    }

    public void setIdCreador(Long idCreador) {
        this.idCreador = idCreador;
    }

    public Integer getPremiumCreador() {
        return premiumCreador;
    }

    public String getUsernameCreador() {
        return usernameCreador;
    }

    public void setUsernameCreador(String usernameCreador) {
        this.usernameCreador = usernameCreador;
    }

    public void setPremiumCreador(Integer premiumCreador) {
        this.premiumCreador = premiumCreador;
    }

    public String getNombreCreador() {
        return nombreCreador;
    }

    public void setNombreCreador(String nombreCreador) {
        this.nombreCreador = nombreCreador;
    }

    public String getApellidosCreador() {
        return apellidosCreador;
    }

    public void setApellidosCreador(String apellidosCreador) {
        this.apellidosCreador = apellidosCreador;
    }

    public String getImagenCreador() {
        return imagenCreador;
    }

    public void setImagenCreador(String imagenCreador) {
        this.imagenCreador = imagenCreador;
    }

    public Integer getCountUsuarios() {
        return countUsuarios;
    }

    public void setCountUsuarios(Integer countUsuarios) {
        this.countUsuarios = countUsuarios;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public List<ParticipanteDto> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(List<ParticipanteDto> participantes) {
        this.participantes = participantes;
    }

    public List<ComentarioDto> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<ComentarioDto> comentarios) {
        this.comentarios = comentarios;
    }
}

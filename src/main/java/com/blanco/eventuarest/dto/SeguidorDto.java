package com.blanco.eventuarest.dto;

import java.util.Date;

public class SeguidorDto {

    private Long id;
    private String username;
    private String nombre;
    private String apellido;
    private Date fechaSeguido;
    private String imagen;
    private String cp;
    private String provincia;
    private String poblacion;

    public SeguidorDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechaSeguido() {
        return fechaSeguido;
    }

    public void setFechaSeguido(Date fechaSeguido) {
        this.fechaSeguido = fechaSeguido;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

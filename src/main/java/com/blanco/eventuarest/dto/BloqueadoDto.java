package com.blanco.eventuarest.dto;

import java.util.Date;

public class BloqueadoDto {

    private Long id;
    private String username;
    private String nombre;
    private String apellido;
    private Date fechaBloqueado;
    private String imagen;
    private String cp;
    private String poblacion;
    private String provincia;

    public BloqueadoDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechaBloqueado() {
        return fechaBloqueado;
    }

    public void setFechaBloqueado(Date fechaBloqueado) {
        this.fechaBloqueado = fechaBloqueado;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
}

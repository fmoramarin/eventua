package com.blanco.eventuarest.dto;

public class PremiumDto {
    private Long idUsuario;
    private Long idPremium;

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getIdPremium() {
        return idPremium;
    }

    public void setIdPremium(Long idPremium) {
        this.idPremium = idPremium;
    }
}

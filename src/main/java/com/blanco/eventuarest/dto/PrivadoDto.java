package com.blanco.eventuarest.dto;

import com.blanco.eventuarest.models.Privado;
import org.apache.tomcat.jni.Local;

import java.time.LocalDateTime;
import java.util.List;

public class PrivadoDto {
    private Long id;
    private Long idReceptor;
    private String nombre;
    private String apellidos;
    private LocalDateTime ultimaConexion;
    private String imagen;
    private int leido;
    private String mensajesEmisor;
    private LocalDateTime fechaMensaje;
    private boolean conectado;
    private String ultMensaje;
    private int contLeido;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdReceptor() {
        return idReceptor;
    }

    public void setIdReceptor(Long idReceptor) {
        this.idReceptor = idReceptor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public LocalDateTime getUltimaConexion() {
        return ultimaConexion;
    }

    public void setUltimaConexion(LocalDateTime ultimaConexion) {
        this.ultimaConexion = ultimaConexion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public int getLeido() {
        return leido;
    }

    public void setLeido(int leido) {
        this.leido = leido;
    }

    public String getMensajesEmisor() {
        return mensajesEmisor;
    }

    public void setMensajesEmisor(String mensajesEmisor) {
        this.mensajesEmisor = mensajesEmisor;
    }

    public LocalDateTime getFechaMensaje() {
        return fechaMensaje;
    }

    public void setFechaMensaje(LocalDateTime fechaMensaje) {
        this.fechaMensaje = fechaMensaje;
    }

    public boolean isConectado() {
        return conectado;
    }

    public void setConectado(boolean conectado) {
        this.conectado = conectado;
    }

    public String getUltMensaje() {
        return ultMensaje;
    }

    public void setUltMensaje(String ultMensaje) {
        this.ultMensaje = ultMensaje;
    }

    public int getContLeido() {
        return contLeido;
    }

    public void setContLeido(int contLeido) {
        this.contLeido = contLeido;
    }
}

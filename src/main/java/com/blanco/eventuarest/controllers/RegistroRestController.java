package com.blanco.eventuarest.controllers;

import com.blanco.eventuarest.services.RegistroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins={"http://localhost:4200"})
@RestController
@RequestMapping("/eventua")
public class RegistroRestController {
    @Autowired
    private RegistroService registroService;

    @PostMapping("/registro/comprobarusuario/{username}")
    public boolean comprobarUsuario(@PathVariable String username) {
        return this.registroService.comprobarUsername(username);
    }

    @PostMapping("/registro/comprobaremail/{email}")
    public boolean comprobarEmail(@PathVariable String email) {
        return this.registroService.comprobarEmail(email);
    }
}

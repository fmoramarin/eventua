package com.blanco.eventuarest.controllers;

import com.blanco.eventuarest.dto.PremiumDto;
import com.blanco.eventuarest.models.Premium;
import com.blanco.eventuarest.services.PremiumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins={"http://localhost:4200"})
@RestController
@RequestMapping("/eventua")
public class PremiumRestController {
    @Autowired
    PremiumService premiumService;

    @GetMapping("/premium/{plan}")
    public Premium findByPlan(@PathVariable String plan) {
        return this.premiumService.findByPlan(plan);
    }

    @PutMapping("/premium/ser-premium")
    public void setPremium(@RequestBody PremiumDto premiumDto) {
        this.premiumService.serPremium(premiumDto);
    }
}

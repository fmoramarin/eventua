package com.blanco.eventuarest.controllers;

import com.blanco.eventuarest.dto.BloqueadoDto;
import com.blanco.eventuarest.dto.PasswordDto;
import com.blanco.eventuarest.dto.PerfilDto;
import com.blanco.eventuarest.dto.RegistroDto;
import com.blanco.eventuarest.models.*;
import com.blanco.eventuarest.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins={"http://localhost:4200"})
@RestController
@RequestMapping("/eventua")
public class UsuarioRestController {
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private InteresService interesService;
    @Autowired
    private BloqueadoService bloqueadoService;
    @Autowired
    private CPService cpService;


    @GetMapping("/usuarios")
    public List<Usuario> listarUsuarios() {
        return usuarioService.findAll();
    }

    @GetMapping("/usuarios/{id}")
    public Usuario mostrar(@PathVariable Long id) {
        return usuarioService.findById(id);
    }

    @PostMapping(value="/imagenes/usuario")
    public void uploadImg(@RequestParam("imagen") MultipartFile multipartFile) throws  Exception {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        String uploadDir = "user-photos/";
        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
    }

    @PostMapping(value = "/usuarios/insertar")
    @ResponseStatus(HttpStatus.CREATED)
    public void insertar(@RequestBody RegistroDto registro) throws Exception {
        // objeto equivalente a la tabla en la BBDD
        Usuario usuarioInsert = new Usuario();
        // lista que contiene todos los objetos intereses, formados por el usuario y la categoría
        List<Interes> interesFull = new ArrayList<>();
        // lista que contiene todas las categorías que el usuario ha pulsado
        List<Categoria> categoria = new ArrayList<>();
        // creamos el objeto de la relación y le seteamos su valor llamando al setter
        CP cp = new CP();
        cp.setCp(registro.getCp());
        LocalDateTime fecha = LocalDateTime.now();

        // seteamos todos los valores de la tabla Usuario buscándolo en el DTO
        usuarioInsert.setRol(2);
        usuarioInsert.setEstado(1);
        usuarioInsert.setPremium(0);
        usuarioInsert.setUser(registro.getUsername());
        usuarioInsert.setEmail(registro.getEmail());
        usuarioInsert.setPassword(EncriptarPasswordService.encriptar(registro.getPassword()));
        usuarioInsert.setNombre(registro.getNombre());
        usuarioInsert.setApellidos(registro.getApellidos());
        usuarioInsert.setFechaNacimiento(registro.getFechaNacimiento());
        usuarioInsert.setTelefono(registro.getTelefono());
        usuarioInsert.setCp(cp);
        usuarioInsert.setDescripcion(registro.getDescripcion());
        usuarioInsert.setImagen(registro.getImagen());
        usuarioInsert.setFechaInscripcion(new Date());
        usuarioInsert.setUltimaConexion(fecha);

        // guardamos el usuario en la BBDD
        usuarioService.save(usuarioInsert);

        //buscamos el usuario que acabamos de registrar para poder obtener su id.
        Usuario usuario = usuarioService.findByUsername(registro.getUsername());

        // introducir en la lista categoria el array de checkbox que viene de RegistroDTO
        for(Long i:registro.getCheckbox()) {
            Categoria cat = new Categoria();
            cat.setId(i);
            categoria.add(cat);
        }

        // introducir en la lista de interesFull los objetos interesInsert que se van creando en cada loop
       for (Categoria cat:categoria) {
            Interes interesInsert = new Interes();
            interesInsert.setUsuario(usuario);
            interesInsert.setCategoria(cat);
            interesFull.add(interesInsert);
        }

       // guardamos los intereses en la BBDD
       interesService.saveAll(interesFull);

    }

    @PutMapping("/usuarios/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario actualizar(@RequestBody Usuario usuario, @PathVariable Long id) {
        Usuario usuarioActual = usuarioService.findById(id);
        usuarioActual.setNombre(usuario.getNombre());
        // Aquí irían todos los update que se necesite

        return usuarioService.save(usuarioActual);
    }

    @PostMapping(value="/pruebaregistro")
    @ResponseStatus(HttpStatus.CREATED)
    public String llegada(@RequestBody Usuario usuario) {
        return usuario.getNombre();
    }

    @DeleteMapping("/usuarios/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrar(@PathVariable Long id) {
        usuarioService.delete(id);
    }

    @GetMapping("/perfil/{username}")
    public RegistroDto mostrarPerfil (@PathVariable String username) {
        Usuario usuario = usuarioService.findByUsername(username);
        RegistroDto perfil = new RegistroDto();

        List<Interes> interesesList = interesService.findByUsuario(usuario);
        List<String> intereses = new ArrayList<>();

        perfil.setId(usuario.getId());
        perfil.setNombre(usuario.getNombre());
        perfil.setApellidos(usuario.getApellidos());
        perfil.setEmail(usuario.getEmail());
        perfil.setTelefono(usuario.getTelefono());
        perfil.setMunicipio(usuario.getCp().getPoblacion());
        perfil.setUsername(usuario.getUsername());
        perfil.setDescripcion(usuario.getDescripcion());
        perfil.setFechaInscripcion(usuario.getFechaInscripcion());
        perfil.setImagen(usuario.getImagen());
        perfil.setFechaNacimiento(usuario.getFechaNacimiento());
        perfil.setCp(usuario.getCp().getCp());
        perfil.setPremium(usuario.getPremium());

        for (Interes interes : interesesList) {
            if (interes.getCategoria().getSubcategoria().equals("")) {
                intereses.add(interes.getCategoria().getCategoria());
            } else {
                intereses.add(interes.getCategoria().getSubcategoria());
            }
        }

        perfil.setIntereses(intereses);
        return perfil;
    }

    @PutMapping("/cambiar-password")
    public boolean cambiarPassword (@RequestBody PasswordDto passwordDto) throws Exception {
        Usuario usuario = usuarioService.findById(passwordDto.getIdSesion());
        if (usuario.getPassword().equals(EncriptarPasswordService.encriptar(passwordDto.getOldPassword()))) {
            usuario.setPassword(EncriptarPasswordService.encriptar(passwordDto.getNewPassword()));
            this.usuarioService.save(usuario);
            return true;
        } else {
            return false;
        }
    }

    @PostMapping("/bloquear/{bloqueador}/{bloqueado}")
    public void bloquearUsuario (@PathVariable String bloqueador, @PathVariable String bloqueado) {
        this.bloqueadoService.bloquear(bloqueador, bloqueado);
    }

    @DeleteMapping("/desbloquear/{bloqueador}/{bloqueado}")
    public void desbloquearUsuario (@PathVariable String bloqueador, @PathVariable String bloqueado) {
        this.bloqueadoService.desbloquear(bloqueador, bloqueado);
    }

    @GetMapping("/comprobar-bloqueo/{bloqueador}/{bloqueado}")
    public boolean comprobarBloqueo (@PathVariable String bloqueador, @PathVariable String bloqueado) {
        return this.bloqueadoService.buscarBloqueo(bloqueador, bloqueado);
    }

    @GetMapping("/bloqueados/{username}")
    public List<BloqueadoDto> mostrarBloqueados (@PathVariable String username) {
        List<Bloqueado> bloqueados = this.bloqueadoService.listarBloqueados(username);
        List<BloqueadoDto> bloqueadosList = new ArrayList<>();
        for (Bloqueado bloqueado : bloqueados) {
            BloqueadoDto usuBloqueado = new BloqueadoDto();

            usuBloqueado.setUsername(bloqueado.getBloqueado().getUsername());
            usuBloqueado.setNombre(bloqueado.getBloqueado().getNombre());
            usuBloqueado.setApellido(bloqueado.getBloqueado().getApellidos());
            usuBloqueado.setFechaBloqueado(bloqueado.getFechaBloqueo());
            usuBloqueado.setImagen(bloqueado.getBloqueado().getImagen());
            usuBloqueado.setCp(bloqueado.getBloqueado().getCp().getCp());
            usuBloqueado.setPoblacion(bloqueado.getBloqueado().getCp().getPoblacion());
            usuBloqueado.setProvincia(bloqueado.getBloqueado().getCp().getProvincia());

            bloqueadosList.add(usuBloqueado);
        }
        return bloqueadosList;
    }

    @PutMapping("/editar-perfil")
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario actualizar(@RequestBody PerfilDto perfilDto) {
        Usuario usuario = this.usuarioService.findByUsername(perfilDto.getUsername());

        System.out.println(perfilDto.getNombre());

        CP cp = new CP();
        cp.setCp(perfilDto.getCp());

        usuario.setNombre(perfilDto.getNombre());
        usuario.setApellidos(perfilDto.getApellidos());
        usuario.setTelefono(perfilDto.getTelefono());
        usuario.setImagen(perfilDto.getImagen());
        usuario.setDescripcion(perfilDto.getDescripcion());
        usuario.setCp(this.cpService.findById(cp.getCp()));

        return this.usuarioService.save(usuario);
    }

    @PutMapping("usuario/ultima-conexion/{id}")
    public void actualizarConexion(@PathVariable Long id) {
        Usuario usuario = this.usuarioService.findById(id);
        usuario.setUltimaConexion(LocalDateTime.now());
        this.usuarioService.save(usuario);
    }

    @GetMapping("usuario/buscar/{nombre}")
    public List<PerfilDto> buscarUsuario(@PathVariable String nombre) {
        return this.usuarioService.findByNombreAndApellidos(nombre);
    }
}

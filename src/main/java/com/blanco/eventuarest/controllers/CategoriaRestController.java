package com.blanco.eventuarest.controllers;

import com.blanco.eventuarest.models.Categoria;
import com.blanco.eventuarest.models.Interes;
import com.blanco.eventuarest.services.CategoriaService;
import com.blanco.eventuarest.services.InteresService;
import com.blanco.eventuarest.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins={"http://localhost:4200"})
@RestController
@RequestMapping("/eventua")
public class CategoriaRestController {
    @Autowired
    CategoriaService categoriaService;
    @Autowired
    InteresService interesService;
    @Autowired
    UsuarioService usuarioService;

    @GetMapping("/categorias")
    public List<Categoria> listarCategorias() {
        return this.categoriaService.getCategorias();
    }

    @GetMapping("/subcategorias")
    public List<String> listarSubcategorias() {
        List<Categoria> categorias = this.categoriaService.getCategorias();
        List<String> subcategorias = new ArrayList<>();
        for (Categoria categoria:categorias) {
            if (!categoria.getSubcategoria().equals("") && !categoria.getSubcategoria().equals("otros")) {
                subcategorias.add(categoria.getSubcategoria());
            }
        }
        return subcategorias;
    }

    /*@GetMapping("/categorias/categorias")
    public List<Map<String,Object>> getCategorias() {
        List<Map<String,Object>> lista = new ArrayList<>();
        for(Categoria categoria:this.categoriaService.listarCategorias()) {
            Map<String,Object> mapa = new HashMap<>();
            mapa.put("id",categoria.getId());
            mapa.put("categoria",categoria.getCategoria());
            lista.add(mapa);
        }
        return lista;
    }

    @GetMapping("/categorias/subcategoria/{categoria}")
    public List<Map<String,Object>> getSubcategorias(@PathVariable String categoria) {
        List<Map<String,Object>> lista = new ArrayList<>();
        for (Categoria subcategoria:this.categoriaService.listarSubCategorias(categoria)) {
            Map<String,Object> mapa = new HashMap<>();
            mapa.put("id",subcategoria.getId());
            mapa.put("subcategoria",subcategoria.getSubcategoria());
            lista.add(mapa);
        }
        return lista;
    }*/

    @GetMapping("/perfil/intereses/{username}")
    public List<String> intereses (@PathVariable String username) {
        List<Interes> interesList = this.interesService.findByUsuario(this.usuarioService.findByUsername(username));
        List<String> intereses = new ArrayList<>();
        for (Interes interes : interesList) {
            String categoria;
            if (interes.getCategoria().getSubcategoria().equals("")) {categoria = interes.getCategoria().getCategoria();}
            else {categoria = interes.getCategoria().getSubcategoria();}
            intereses.add(categoria);
        }
        return intereses;
    }
}

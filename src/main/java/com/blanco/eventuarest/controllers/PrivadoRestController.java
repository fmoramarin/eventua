package com.blanco.eventuarest.controllers;

import com.blanco.eventuarest.dto.PrivadoDto;
import com.blanco.eventuarest.services.PrivadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins={"http://localhost:4200"})
@RestController
@RequestMapping("/eventua")
public class PrivadoRestController {
    @Autowired
    PrivadoService privadoService;

    @GetMapping("/mensajes/{emisor}/{receptor}")
    public List<PrivadoDto> getMensajes(@PathVariable Long emisor, @PathVariable Long receptor) {
        return this.privadoService.getMensajes(emisor, receptor);
    }

    @GetMapping("/mensajes/{emisor}")
    public List<PrivadoDto> getChat(@PathVariable Long emisor) {
        return this.privadoService.getChat(emisor);
    }

    @PostMapping("/mensajes/insertar")
    public void setMensaje(@RequestBody PrivadoDto privadoDto) {
        this.privadoService.guardarMensaje(privadoDto);
    }

    @PutMapping("/mensajes/leidos")
        public void mensajesLeidos(@RequestBody PrivadoDto privadoDto) {
            this.privadoService.mensajesLeidos(privadoDto);
        }
}

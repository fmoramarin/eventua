package com.blanco.eventuarest.controllers;

import com.blanco.eventuarest.dto.SeguidorDto;
import com.blanco.eventuarest.models.CP;
import com.blanco.eventuarest.models.Seguidor;
import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.services.CPService;
import com.blanco.eventuarest.services.SeguidorService;
import com.blanco.eventuarest.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins={"http://localhost:4200"})
@RestController
@RequestMapping("/eventua")
public class SeguidorRestController {

    @Autowired
    private SeguidorService seguidorService;

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/perfil/seguidores/{username}")
    public List<SeguidorDto> mostrarSeguidores (@PathVariable String username) {
        System.out.println(username);
        Usuario usuario = usuarioService.findByUsername(username);
        System.out.println(usuarioService.findByUsername(username));
        List<Seguidor> seguidoresList = seguidorService.findBySeguido(usuario);
        List<SeguidorDto> seguidores = new ArrayList<>();

        for (Seguidor seguidor : seguidoresList) {
            Usuario usuarioSeguidor = usuarioService.findById(seguidor.getSeguidor().getId());

            SeguidorDto seguidorDto = new SeguidorDto();
            seguidorDto.setId(usuarioSeguidor.getId());
            seguidorDto.setNombre(usuarioSeguidor.getNombre());
            seguidorDto.setUsername(usuarioSeguidor.getUsername());
            seguidorDto.setApellido(usuarioSeguidor.getApellidos());
            seguidorDto.setFechaSeguido(seguidor.getFecha());
            seguidorDto.setImagen(usuarioSeguidor.getImagen());
            seguidorDto.setCp(usuarioSeguidor.getCp().getCp());
            seguidorDto.setPoblacion(usuarioSeguidor.getCp().getPoblacion());
            seguidorDto.setProvincia(usuarioSeguidor.getCp().getProvincia());

            seguidores.add(seguidorDto);
        }

        return seguidores;
    }

    @GetMapping("/perfil/seguidos/{username}")
    public List<SeguidorDto> mostrarSeguidos (@PathVariable String username) {
        Usuario usuario = usuarioService.findByUsername(username);
        List<Seguidor> seguidosList = seguidorService.findBySeguidor(usuario);
        List<SeguidorDto> seguidos = new ArrayList<>();

        for (Seguidor Seguido : seguidosList) {
            Usuario usuarioSeguido = usuarioService.findById(Seguido.getSeguido().getId());

            SeguidorDto seguidoDto = new SeguidorDto();
            seguidoDto.setId(usuarioSeguido.getId());
            seguidoDto.setUsername(usuarioSeguido.getUsername());
            seguidoDto.setNombre(usuarioSeguido.getNombre());
            seguidoDto.setApellido(usuarioSeguido.getApellidos());
            seguidoDto.setFechaSeguido(Seguido.getFecha());
            seguidoDto.setImagen(usuarioSeguido.getImagen());
            seguidoDto.setCp(usuarioSeguido.getCp().getCp());
            seguidoDto.setPoblacion(usuarioSeguido.getCp().getPoblacion());
            seguidoDto.setProvincia(usuarioSeguido.getCp().getProvincia());

            seguidos.add(seguidoDto);
        }

        return seguidos;
    }

    @PostMapping("/nuevoseguidor/{seguidor}/{seguido}")
    public void marcarSeguidor (@PathVariable String seguidor, @PathVariable String seguido) {
        this.seguidorService.save(seguidor, seguido);
//        if (!(this.seguidorService.save(seguidor,seguido) == null)) {return true;}
//        else {return false;}
    }

    @DeleteMapping("/eliminarseguidor/{seguidor}/{seguido}")
    public void eliminarSeguidor(@PathVariable String seguidor, @PathVariable String seguido) {
        Usuario usuSeguidor = this.usuarioService.findByUsername(seguidor);
        Usuario usuSeguido = this.usuarioService.findByUsername(seguido);

        this.seguidorService.deleteBySeguidorAndSeguido(usuSeguidor, usuSeguido);
    }

}

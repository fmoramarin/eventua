package com.blanco.eventuarest.controllers;

import com.blanco.eventuarest.services.CPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins={"http://localhost:4200"})
@RestController
@RequestMapping("/eventua")
public class CPRestController {
    @Autowired
    CPService cpService;

    @PostMapping("/cp/comprobarcp/{cp}")
    public boolean buscarCP(@PathVariable String cp) {
        return this.cpService.buscarCP(cp);
    }
}

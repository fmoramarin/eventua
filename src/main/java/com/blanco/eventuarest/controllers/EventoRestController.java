package com.blanco.eventuarest.controllers;

import com.blanco.eventuarest.dto.BusquedaDto;
import com.blanco.eventuarest.dto.EventoDto;
import com.blanco.eventuarest.dto.EventoPerfilDto;
import com.blanco.eventuarest.models.*;
import com.blanco.eventuarest.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/eventua")
public class EventoRestController {
    @Autowired
    private EventoService eventoService;
    @Autowired
    private InteresService interesService;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private CPService cpService;
    @Autowired
    private CategoriaService categoriaService;
    @Autowired
    private FavoritoService favoritoService;
    @Autowired
    private ParticipanteService participanteService;
    @Autowired
    private SeguidorService seguidorService;

    @PostMapping(value = "/evento/crear", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public void crearEvento(@RequestBody EventoDto evento) {

        Evento eventoInsert = new Evento();

        Usuario creador = new Usuario();
        creador.setId(evento.getCreador());

        Categoria categoria = new Categoria();
        categoria.setId(evento.getSubcategoria());
        Categoria categoriaImg = this.categoriaService.getCategoria(evento.getSubcategoria());

        CP cp = new CP();
        cp.setCp(evento.getCp());

        eventoInsert.setNombre(evento.getNombre());
        eventoInsert.setDescripcion(evento.getDescripcion());
        eventoInsert.setFechaCreacion(new Date());
        eventoInsert.setFechaProgramada(evento.getFechaProgramada());
        eventoInsert.setFechaLimite(evento.getFechaLimite());
        eventoInsert.setDireccion(evento.getDireccion());
        eventoInsert.setCoste(evento.getPrecio());
        eventoInsert.setNivel(evento.getNivel());
        eventoInsert.setMaxUsuarios(evento.getMaxUsuarios());
        eventoInsert.setVisualizaciones(0);
        eventoInsert.setCategoria(categoria);
        eventoInsert.setCreador(creador);
        eventoInsert.setCp(cp);
        eventoInsert.setReportes(0);

        if (evento.getImagen().isEmpty()) { eventoInsert.setImagen(categoriaImg.getImagen());
            System.out.println("Entro aquí");}
         else eventoInsert.setImagen(evento.getImagen());

        this.eventoService.crearEvento(eventoInsert);
    }

    @GetMapping(value = "/evento/findinteres/{id}")
    public List<EventoDto> eventoPorIntereses(@PathVariable Long id) {
        Usuario usuario = usuarioService.findById(id);
        List<Interes> intereses = this.interesService.findByUsuario(usuario);
        List<Evento> eventosList = new ArrayList<>();

        for (Interes interes : intereses) {
            List<Evento> eventos = this.eventoService.findByCategoria(interes.getCategoria());
            if (!eventos.isEmpty()) {
                for (Evento evento : eventos) {
                    if (evento.getCreador().getId().equals(usuario.getId())) continue;
                    eventosList.add(evento);
                }
            }
        }
        return eventoService.eventoRandom(eventoService.eventoAEventoDto(eventosList));
    }

    @PostMapping(value = "/imagenes/evento")
    public void uploadImg(@RequestParam("imagen") MultipartFile multipartFile) throws Exception {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        String uploadDir = "event-photo/";
        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
    }

    //Mostrar eventos premium
    @GetMapping(value = "/evento/findpremium")
    public List<EventoDto> eventoPorPremium() {
        List<Usuario> usuariosList = this.usuarioService.findByPremium(1);
        List<Evento> allEventosPremium = new ArrayList<>();

        for (Usuario usuario : usuariosList) {
            List<Evento> eventosPremium = eventoService.findByCreador(usuario);
            if (!eventosPremium.isEmpty()){
                for (Evento evento : eventosPremium) {
//                    if (evento.getCreador().getId().equals(usuario.getId())) continue;
                    allEventosPremium.add(evento);
                }
            }
        }
        return eventoService.eventoAEventoDto(eventoService.eventoRandomPremium(allEventosPremium));
    }

    @GetMapping(value = "/evento/findcercanos/{id}")
    public List<EventoDto> eventosPorCercania(@PathVariable Long id) {

        Usuario usuario = this.usuarioService.findById(id);
        CP codigoPostal = this.cpService.findById(usuario.getCp().getCp());
        List<CP> listaCP = this.cpService.findByProvincia(codigoPostal.getProvincia());
        List<Evento> eventos = new ArrayList<>();
        for (CP cp : listaCP) {
            List<Evento> eventosCp = this.eventoService.findByCp(cp);
            if (!eventosCp.isEmpty()){
                for (Evento evento : eventosCp) {
                    if (evento.getCreador().getId().equals(usuario.getId())) continue;
                    eventos.add(evento);
                }
            }
        }
        return eventoService.eventoRandom(eventoService.eventoAEventoDto(eventos));
    }

    @GetMapping("/favoritos/{username}")
    public List<EventoPerfilDto> eventosFavoritos (@PathVariable String username) {
        Usuario usuario = usuarioService.findByUsername(username);
        List<Favorito> favoritos = this.favoritoService.listByUsuario(usuario);
        List<Evento> eventos = new ArrayList<>();

        for (Favorito favorito : favoritos) {
            Evento evento = eventoService.findById(favorito.getEvento().getId());
            eventos.add(evento);
        }

        return this.eventoService.eventoAEventoPerfilDto(eventos);

    }

    @GetMapping("/eventospropios/{username}")
    public List<EventoPerfilDto> eventosPropios (@PathVariable String username) {
        Usuario usuario = this.usuarioService.findByUsername(username);
        List<Evento> eventos = this.eventoService.findByCreador(usuario);

        return this.eventoService.eventoAEventoPerfilDto(eventos);
    }

    @GetMapping("/eventosapuntado/{username}")
    public List<EventoPerfilDto> eventosApuntado (@PathVariable String username) {
        Usuario usuario = this.usuarioService.findByUsername(username);
        List<Participante> participantes = this.participanteService.findByParticipante(usuario);
        List<Evento> eventos = new ArrayList<>();

        for (Participante participante : participantes) {
            Evento evento = eventoService.findById(participante.getEvento().getId());
            eventos.add(evento);
        }
        return this.eventoService.eventoAEventoPerfilDto(eventos);
    }

    @GetMapping(value= "/evento/findseguidos/{id}")
    public List<EventoDto> eventosSeguidos(@PathVariable Long id){
        Usuario usuario= this.usuarioService.findById(id);
        List<Seguidor> listaSeguidos = this.seguidorService.findBySeguidor(usuario);
        List<Evento> eventos= new ArrayList<>();

        for(Seguidor seguidor: listaSeguidos){
            List<Evento> eventosSeguidos =this.eventoService.findByCreador(seguidor.getSeguido());

            if(!eventosSeguidos.isEmpty()){
                for (Evento evento : eventosSeguidos){
                    //if (evento.getCreador().getId().equals(usuario.getId())) continue;
                    eventos.add(evento);
                }
            }
        }
        return this.eventoService.eventoRandom(this.eventoService.eventoAEventoDto(eventos));
    }

    @PostMapping("/all-eventos")
    public List<EventoPerfilDto> listarEventos (@RequestBody BusquedaDto busquedaDto) {
        List<Evento> eventos = new ArrayList<>();

        if (busquedaDto.getCategorias().length != 0) {
            eventos = this.eventoService.filtroCategorias(busquedaDto);
        } else if (busquedaDto.getCp() != null) {
            CP cp = this.cpService.findById(busquedaDto.getCp());
            eventos = this.eventoService.findByCp(cp);
        } else if (busquedaDto.getProvincia() != null) {
            List<CP> cpList = this.cpService.findByProvincia(busquedaDto.getProvincia());
            for (CP cp : cpList) {
                eventos.addAll(this.eventoService.findByCp(cp));
            }
        } else if (busquedaDto.getFechaProgramada() != null) {
            eventos = this.eventoService.findByFecha(busquedaDto.getFechaProgramada());
        } else if (busquedaDto.getCoste() != null) {
            if (busquedaDto.getCoste() != 0) {
                eventos = this.eventoService.filtroCoste(busquedaDto.getCoste());
            } else {
                eventos = this.eventoService.findByCoste(busquedaDto.getCoste());
            }
        } else {
            eventos = this.eventoService.findAll();
        }
        return this.eventoService.eventoAEventoPerfilDto(this.eventoService.eventosPorUsuario(eventos, busquedaDto.getCasoUsuario(), busquedaDto.getIdSesion()));
    }

    @PostMapping("/all-eventosNombre")
    public List<EventoPerfilDto> findByNombre(@RequestBody String nombre) {

        List<Evento> eventos = this.eventoService.findByNombre(nombre);
        return this.eventoService.eventoAEventoPerfilDto(eventos);
    }

}

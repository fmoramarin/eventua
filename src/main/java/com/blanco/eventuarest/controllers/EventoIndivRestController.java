package com.blanco.eventuarest.controllers;

import com.blanco.eventuarest.dto.*;
import com.blanco.eventuarest.models.*;
import com.blanco.eventuarest.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@CrossOrigin(origins={"http://localhost:4200"})
@RestController
@RequestMapping("/eventua")
public class EventoIndivRestController {

    @Autowired
    private EventoIndivService eventoIndivService;
    @Autowired
    private ParticipanteService participanteService;
    @Autowired
    private ComentarioService comentarioService;
    @Autowired
    private FavoritoService favoritoService;

    @GetMapping("evento/{id}")
    public EventoIndivDto getEventoIndiv(@PathVariable Long id) {
        Evento eventoBase = this.eventoIndivService.findById(id);

        int totalAdmitidos = this.participanteService.totalAdmitidos(1, eventoBase);

        EventoIndivDto evento = new EventoIndivDto();
        evento.setId(eventoBase.getId());
        evento.setNombre(eventoBase.getNombre());
        evento.setImagen(eventoBase.getImagen());
        evento.setDescripcion(eventoBase.getDescripcion());
        evento.setFechaCreacion(eventoBase.getFechaCreacion());
        evento.setFechaProgramada(eventoBase.getFechaProgramada());
        evento.setFechaLimite(eventoBase.getFechaLimite());
        evento.setDireccion(eventoBase.getDireccion());
        evento.setCoste(eventoBase.getCoste());
        evento.setMaxUsuarios(eventoBase.getMaxUsuarios());

        evento.setCountUsuarios(evento.getMaxUsuarios() - totalAdmitidos);

        evento.setNivel(eventoBase.getNivel());
        evento.setCategoria(eventoBase.getCategoria().getSubcategoria());
        evento.setPoblacion(eventoBase.getCp().getPoblacion());
        evento.setProvincia(eventoBase.getCp().getProvincia());

        evento.setIdCreador(eventoBase.getCreador().getId());
        evento.setUsernameCreador(eventoBase.getCreador().getUsername());
        evento.setPremiumCreador(eventoBase.getCreador().getPremium());
        evento.setNombreCreador(eventoBase.getCreador().getNombre());
        evento.setApellidosCreador(eventoBase.getCreador().getApellidos());
        evento.setImagenCreador(eventoBase.getCreador().getImagen());

        List<Participante> participanteList = this.participanteService.findByEvento(eventoBase);
        List<ParticipanteDto> participantes = new ArrayList<>();

        for (Participante participante : participanteList) {
            ParticipanteDto participanteDto = new ParticipanteDto();
            participanteDto.setId(participante.getId());
            participanteDto.setIdUsuario(participante.getParticipante().getId());
            participanteDto.setUsernameUsuario(participante.getParticipante().getUsername());
            participanteDto.setNombre(participante.getParticipante().getNombre());
            participanteDto.setApellido(participante.getParticipante().getApellidos());
            participanteDto.setImagen(participante.getParticipante().getImagen());
            participanteDto.setPremium(participante.getParticipante().getPremium());
            participanteDto.setFecha(participante.getFechaInscripcion());
            participanteDto.setAdmitido(participante.getAdmitido());

            participantes.add(participanteDto);
        }
        evento.setParticipantes(participantes);

        List<Comentario> comentarioList = this.comentarioService.findByEvento(eventoBase);
        List<ComentarioDto> comentarios = new ArrayList<>();

        for (Comentario comentario : comentarioList) {
            ComentarioDto comentarioDto = new ComentarioDto();
            comentarioDto.setId(comentario.getId());
            comentarioDto.setComentario(comentario.getComentario());
            comentarioDto.setFecha(comentario.getFecha());
            comentarioDto.setNombre(comentario.getUsuario().getNombre());
            comentarioDto.setUsernameUsuario(comentario.getUsuario().getUsername());
            comentarioDto.setApellidos(comentario.getUsuario().getApellidos());
            comentarioDto.setImagen(comentario.getUsuario().getImagen());
            comentarioDto.setPremium(comentario.getUsuario().getPremium());

            comentarios.add(comentarioDto);
        }

        evento.setComentarios(comentarios);
        return evento;
    }

    @GetMapping("evento/no-admitidos/{id}")
    public List<ParticipanteDto> buscarNoAdmitidos(@PathVariable Long id) {
        Evento evento = eventoIndivService.findById(id);
        List<Participante> participantes = this.participanteService.findByAdmitidoAndEvento(0,evento);
        List<ParticipanteDto> participanteDtos = new ArrayList<>();
        for (Participante participante:participantes) {
            ParticipanteDto participanteDto = new ParticipanteDto();
            participanteDto.setIdUsuario(participante.getParticipante().getId());
            participanteDto.setId(participante.getId());
            participanteDto.setImagen(participante.getParticipante().getImagen());
            participanteDto.setUsernameUsuario(participante.getParticipante().getUsername());
            participanteDto.setNombre(participante.getParticipante().getNombre());
            participanteDto.setApellido(participante.getParticipante().getApellidos());
            participanteDto.setFecha(participante.getFechaInscripcion());
            participanteDto.setAdmitido(participante.getAdmitido());
            participanteDtos.add(participanteDto);
        }
        return participanteDtos;
    }

    @PostMapping("evento/publicar")
    public void publicarComentario(@RequestBody ComentarioDto comentario) {
        this.comentarioService.publicarComentario(comentario);
    }

    @PostMapping("evento/apuntarse")
    public void apuntarEvento(@RequestBody ParticipanteDto participanteDto) {
        this.participanteService.save(participanteDto);
    }

    @PostMapping("evento/retirarse")
    public Long retirarse(@RequestBody ParticipanteDto participanteDto) {
        return this.participanteService.retirarse(participanteDto);
    }

    @PostMapping("evento/set-favorito")
    public void darFavorito(@RequestBody FavoritoDto favoritoDto) {
        this.favoritoService.save(favoritoDto);
    }

    @PostMapping("evento/quit-favorito")
    public Long retirarse(@RequestBody FavoritoDto favoritoDto) {
        return this.favoritoService.retirarFav(favoritoDto);
    }

    @PostMapping("evento/find-favorito")
    public Boolean findFavorito(@RequestBody FavoritoDto favoritoDto) {
        Usuario usuario = new Usuario();
        usuario.setId(favoritoDto.getIdUsuario());
        Evento evento = new Evento();
        evento.setId(favoritoDto.getIdEvento());

        if (this.favoritoService.findFav(usuario, evento).isPresent()) return true;
        else return false;
    }

    @PutMapping("/borrar-comentario/{id}")
    //@ResponseStatus(HttpStatus.CREATED)
    public void borrarComentario (@PathVariable Long id) {
        Comentario comentario = this.comentarioService.findById(id);
        comentario.setComentario("Este comentario ha sido borrado por el creador del evento");
        this.comentarioService.save(comentario);
    }

    @PutMapping("evento/aceptar-participante/{id}")
    public void aceptarParticipante(@PathVariable Long id) {
        this.participanteService.aceptarParticipante(id);
    }

    @PostMapping("evento/borrar-participante/{id}")
    public void eliminarParticipante(@PathVariable Long id) {
        this.participanteService.eliminarParticipante(id);
    }

}

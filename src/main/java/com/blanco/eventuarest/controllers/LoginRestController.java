package com.blanco.eventuarest.controllers;

import com.blanco.eventuarest.models.Usuario;
import com.blanco.eventuarest.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin(origins={"http://localhost:4200"})
@RestController
@RequestMapping("/eventua")
public class LoginRestController {
    @Autowired
    private LoginService loginService;

   /* @PostMapping(value = "/login", produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
    public Map login(@RequestBody LoginForm loginForm) {
        return loginService.verificarLogin(loginForm);
    }*/

    @PostMapping(value = "/login", produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
    public Map login(@RequestBody Usuario usuario) {
        return loginService.verificarLogin(usuario);
    }
}

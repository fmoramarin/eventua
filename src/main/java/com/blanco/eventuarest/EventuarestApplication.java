package com.blanco.eventuarest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventuarestApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventuarestApplication.class, args);
    }

}
